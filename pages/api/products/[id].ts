import type { NextApiRequest, NextApiResponse } from "next";
// import prisma from "../../../../../test-next/my-app/lib/prisma";
import prisma from "../../../lib/prisma";

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
    const { id } = req.query;

    if (req.method === "GET") {
        const product = await prisma.product.findUnique({
            where: { id: parseInt(id as string) },
        });
        res.json(product);
    } else if (req.method === "PUT") {
        const { name, description, price, imageUrl } = req.body;
        const product = await prisma.product.update({
            where: { id: parseInt(id as string) },
            data: { name, description, price: parseFloat(price), imageUrl },
        });
        res.json(product);
    } else if (req.method === "DELETE") {
        await prisma.product.delete({
            where: { id: parseInt(id as string) },
        });
        res.json({ message: "Product deleted" });
    } else {
        res.status(405).json({ message: "Method not allowed" });
    }
}
