import type { NextApiRequest, NextApiResponse } from "next";
// import prisma from "../../../../../test-next/my-app/lib/prisma";
import prisma from '../../../lib/prisma';

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
    if (req.method === "GET") {
        const products = await prisma.product.findMany();
        res.json(products);
    } else if (req.method === "POST") {
        const { name, description, price, imageUrl } = req.body;
        const product = await prisma.product.create({
            data: { name, description, price: parseFloat(price), imageUrl },
        });
        res.json(product);
    } else {
        res.status(405).json({ message: "Method not allowed" });
    }
}
